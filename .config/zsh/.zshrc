## zsh-newuser-install
HISTSIZE=1000
SAVEHIST=1000
setopt appendhistory autocd extendedglob nomatch notify
unsetopt beep
bindkey -v

## compinstall
zstyle :compinstall filename $XDG_CONFIG_HOME/zsh/zshrc
autoload -Uz compinit
[ ! -d $XDG_CACHE_HOME/zsh ] && mkdir -p $XDG_CACHE_HOME/zsh
compinit -d $XDG_CACHE_HOME/zsh/zcompdump-$ZSH_VERSION

## Disable flow control
stty -ixon -ixoff

alias tmux='tmux -f $XDG_CONFIG_HOME/tmux/tmuxrc'
alias wget='wget --hsts-file="$XDG_CACHE_HOME/wget-hsts"'
alias mkdir='mkdir --parents'
alias less='less --quit-if-one-screen --RAW-CONTROL-CHARS --chop-long-lines'
alias df='df --human-readable'
alias ls='ls -hFvq --group-directories-first --color'
alias cp='cp --preserve=mode,ownership,timestamps,xattr'
alias tar='tar --xattrs'
alias xclip='xclip -selection clipboard'
alias rsync='rsync --xattrs'
alias chmod='chmod --preserve-root'
alias chown='chown --preserve-root'
alias chgrp='chgrp --preserve-root'
alias sudo='sudo '
alias du='dutree'
alias exa='exa --classify --group-directories-first --colour=always'
alias find='fd'
alias grep='rg'
alias ffs='sudo $(fc -ln -1)'
alias share='chmod -R u+rwX,go+rX,go-w'
alias vpn='sudo openconnect ssl.vpn.polymtl.ca --user=p112047 --authgroup=PolySSL'
alias scm='chibi-scheme -R'
alias todo='rg --color always --heading -N TODO | sed '\''s/.*TODO[^ ]* /    - /g'\'
alias glow='glow --pager --width 80'
alias readme='glow README.md'

function mk { ## Make directory and change into it
  mkdir $1 && cd $1; }

function ix { ## Command line pastebin
  curl --silent --form 'f:1=<-' ix.io | tee >(xclip); }

function uzip { # unzip into a directory of the same name if necessary
  if [[ $# != 1 ]]; then
    echo I need a single argument, the name of the archive to extract;
    return 1;
  fi
  target="${1%.zip}"
  unzip "$1" -d "${target##*/}";}

## Ctrl-Z to both suspend (default) and resume (new)
function ctrl_z { fg; }
zle -N ctrl_z
bindkey "^Z" ctrl_z

## Install zplug if necessary
if [ ! -d "$ZPLUG_HOME" ]; then
  git clone https://github.com/zplug/zplug "$ZPLUG_HOME"
  source "$ZPLUG_HOME/init.zsh"
  zplug update
  zplug --self-manage
fi; source "$ZPLUG_HOME/init.zsh"

## Plugins
zplug "subnixr/minimal"
zplug "zsh-users/zsh-completions", depth:1
zplug "zsh-users/zsh-autosuggestions"
zplug "zsh-users/zsh-syntax-highlighting", defer:2
zplug check || zplug install; zplug load

[ -f ~/.config/fzf.zsh ] && source ~/.config/fzf.zsh

eval "$(direnv hook zsh)"
