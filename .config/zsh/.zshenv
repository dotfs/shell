export VISUAL=vim
export EDITOR=vim

export XDG_CONFIG_HOME=$HOME/.config
export XDG_CACHE_HOME=$HOME/.cache
export XDG_DATA_HOME=$HOME/.local/share
export HISTFILE=$XDG_DATA_HOME/zsh/history
export VIMINIT='let $MYVIMRC="$XDG_CONFIG_HOME/vim/vimrc" | source $MYVIMRC'
export ZPLUG_HOME=$XDG_DATA_HOME/zplug
export ZPLUG_CACHE_DIR=$XDG_CACHE_HOME/zplug
export ZPLUG_REPOS=$ZPLUG_HOME/repos
export ZPLUG_BIN=$ZPLUG_HOME/bin
export LESSHISTFILE=$XDG_CACHE_HOME/less/history
export LESSKEY=$XDG_CACHE_HOME/less/lesskey
export XINITRC=$XDG_CONFIG_HOME/X11/xinitrc
export XAUTHORITY=$XDG_RUNTIME_DIR/Xauthority
export XTHEMES=$XDG_CONFIG_HOME/X11/themes
export WGETRC=$XDG_CONFIG_HOME/wgetrc
export CARGO_HOME=$XDG_DATA_HOME/cargo
export PLTUSERHOME=$XDG_DATA_HOME/racket
export SSH_AUTH_SOCK="$XDG_RUNTIME_DIR/ssh-agent.socket"
export FZF_DEFAULT_COMMAND='fd --type f'
export GOPATH=$HOME

export PATH=$HOME/bin:$PATH:$HOME/.local/bin:$HOME/.local/share/cargo/bin:$XDG_CONFIG_HOME/git/git-toolbelt
